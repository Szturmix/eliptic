import numpy as np
import matplotlib.pyplot as plt

def solution_plot_with_zeros(u,t,sigma, p, zero_indicies, sub_title=""):
    
    plt.plot(t ,u , '-r')
    times = [ t[index] for index in zero_indicies]
    us = [ u[index] for index in zero_indicies]
    plt.plot(times, us, marker='o', markersize=10, color="black")
    plt.grid()
    
    plt.title("Solution of the equation u'' + u = 0 for sigma=" + str(sigma) + ", and p= " + str(p) + "\n" + sub_title)
    plt.xlabel('r')
    plt.ylabel('u(r)')
    plt.savefig("solutionwithzero.png")
    plt.show()


def solution_plot_with_zero(u,t,sigma, p, zero_index, sub_title=""):
    
    plt.plot(t ,u , '-r')
#    plt.plot([t[zero_index]], [u[zero_index]], marker='o', markersize=10, color="black")
    plt.grid()
    
    plt.title("Solution of the equation u'' + u = 0 for sigma=" + str(sigma) + ", and p= " + str(p) + "\n" + sub_title)
    plt.xlabel('r')
    plt.ylabel('u(r)')
    plt.savefig("solutionwithzero.png")
    plt.show()


def subplot_many_solutions(us ,t,sigma, ps, sub_title=""):
    
    plen = len(ps)
    fig, ax = plt.subplots(plen)
    
    for idx, p in enumerate(ps):
        for u in us:
            ax[idx].plot(t ,u , '-r')
    
    plt.grid()
    
    plt.xlabel('r')
    plt.ylabel('u(r)')
    plt.savefig("manysolutions.png")
    plt.show()

