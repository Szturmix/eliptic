import numpy as np
import matplotlib.pyplot as plt

def solution_plot(u, t, sigma, p, subtitle):
    plt.plot(t ,u , '-r')
    plt.grid()
    
    #plt.title("Solution of the equation u'' + u = 0 for sigma=" + str(sigma) + ", and p=" + str(p) + "\n" + subtitle)
    plt.xlabel('r')
    plt.ylabel('u(r)')
    plt.savefig(subtitle + ".png")
    plt.show()
    plt.clf()


def save_solution_plot(u, t, p, subtitle):
    plt.clf()
    plt.plot(t ,u , '-r')
    plt.grid()
    
    plt.title("Solution of the equation u'' + u = 0 "  + ", p=" + str(p) + "\n" + subtitle)
    plt.xlabel('r')
    plt.ylabel('u(r)')
    plt.savefig(subtitle + ".png")

def norm_plot(norm, kappa,sigma, p, n):
    plt.plot(norm, kappa)
    plt.grid()
    plt.title("The norm of the solution against u(r=1) for sigma=" + str(sigma) + ", and p=" + str(p))

    plt.xlabel('L2-norm')
    plt.ylabel('kappa')
    plt.savefig("normplot" + str(n) + ".png")
    plt.show()
    plt.clf()

def norm_subplot(norms, kappas, ps):

    plen = norms.shape[0]
    fig, ax = plt.subplots(plen)
    #fig.suptitle('Kappa against L2-norm for different p')
    
    for idx in range(plen):
        ax[idx].plot(norms[idx,:], kappas[idx,:], c=np.random.rand(3,))
        ax[idx].set_xlabel('$L^{2} \ norm$')
        ax[idx].set_ylabel('$\kappa$')
        title = "p= " + str(ps[idx])
        ax[idx].legend( [title] ,loc="best")
     
    plt.tight_layout()
    plt.subplots_adjust(wspace=0.9, top=0.9)           
    plt.savefig("subplots.png")
    plt.show()
    plt.clf()
