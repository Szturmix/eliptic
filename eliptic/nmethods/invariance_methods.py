import numpy as np

def find_nearest(array,value, epsilon):
    
    zeros = []
    for idx in range(len(array)-1):
        if (array[idx] > 0 and array[idx + 1] < 0) or (array[idx] < 0 and array[idx + 1] > 0): 
            zeros.append(idx)
    return zeros

def scale_solution(t, u, p, lam, epsilon, number=1):
    t_lambda = t/lam
    indicies = find_nearest(u, 0.0,  epsilon)
    return t[0: indicies[number]]/lam, lam**(-2.0/(p-1)) * u[0: indicies[number]]
