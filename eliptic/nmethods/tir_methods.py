import numpy as np
from scipy.integrate import odeint

def f(u, t, sign, p):
    return [u[1], sign*((np.abs(u[0]))**(p-1))*u[0]]

def g(u, r, sign, p, n):
    return [u[1], -u[1]*(n-1)/r-sign*((np.abs(u[0]))**(p-1))*u[0]]

def quadratic_integrate(f, t):
    dt = np.abs(t[1]-t[0])
    res = 0.0
    for y in f:
        res = res + y*dt
    return res

def L2_norm(f, t):
    return quadratic_integrate(f*f, t)

def secants_method(f, x0, x1, epsilon):
    x = x1
    xprev = x0
    error = np.Infinity
    while(error > epsilon):
        dx = x - xprev
        df = (f(x) - f(xprev))
        xprev = x
        x = x - f(x) * dx / df
        error = abs(f(x))
    
    return x
    
def find_kappa(sigma, N, U0, sign, p, t):
    sol = odeint(f, [U0, sigma], t, args=(sign, p))
    kappa = sol[-1:,0]
    return kappa

def find_kappa_n(sigma, N, U0, sign, p, t, n):
    sol = odeint(g, [U0, sigma], t, args=(sign, p, n))
    kappa = sol[-1:,0]
    return kappa

def solve_with_euler(ps, U0, t, sigmas, sign):
    norms = np.zeros((len(ps),len(sigmas)))
    kappas= np.zeros((len(ps),len(sigmas)))

    for i, p in enumerate(ps):
        for j, sigma in enumerate(sigmas):
            sol = odeint(f, [U0, sigma], t, args=(sign, p))
            y = sol[:,0]
            norms[i,j] = L2_norm(y, t)
            kappas[i,j] = sol[-1:,0]
    return kappas, norms

def solve_with_euler_n(ps, U0, t, sigmas, sign, n):
    norms = np.zeros((len(ps),len(sigmas)))
    kappas= np.zeros((len(ps),len(sigmas)))

    for i, p in enumerate(ps):
        for j, sigma in enumerate(sigmas):
            sol = odeint(g, [sigma, U0], t, args=(sign, p, n))
            y = sol[:,0]
            norms[i,j] = L2_norm(y, t)
            kappas[i,j] = sol[-1:,0]
    return kappas, norms