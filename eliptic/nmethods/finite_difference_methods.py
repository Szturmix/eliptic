import numpy as np
from numpy.linalg import inv
import matplotlib.pyplot as plt

def newton_multidim(x_0, F, dF):

    x = x_0
    x_next = x_0
    
    for i in range(0, 30):
        FU = F(x)
        dFU = dF(x)
        dFU_inv = inv(dFU)
        x_temp = x_next

        x_next = x - np.dot(dFU_inv, FU)
        x = x_temp
    
    return x_next

def F(u, p, N):
    N = len(u)
    h = 1/(N + 1)
    ret = np.zeros(N)
    for idx, el in enumerate(u):
        if idx == 0:
            ret[idx] = (u[idx+1] - 2 * u[idx]) / (h**2) + np.abs(u[idx]) ** (p - 1) * u[idx]
        elif idx == N - 1:
            ret[idx] = (- 2 * u[idx] + u[idx - 1]) / (h**2) + np.abs(u[idx]) ** (p - 1) * u[idx]
        elif idx > 0 and idx < N - 1:
            ret[idx] = (u[idx + 1] - 2*u[idx] + u[idx - 1]) / (h**2) + np.abs(u[idx]) ** (p - 1) * u[idx]
    
    return ret
    
def dF(u, p, N):
    N = len(u)
    h = 1.0/(N + 1)
    ret = np.zeros((N,N))
    for i in range(N):
        ret[i,i] = -2.0 / (h**2) + p * (u[i]**2) * abs(u[i])**( p - 3 ) 
        if i > 0:
            ret[i, i - 1] = 1.0 / (h**2)
        if i < N - 1:
            ret[i, i + 1] = 1.0 / (h**2)
    return ret

def solve_with_difference_method(p, N, A, k):
    x = np.linspace(0, 1, N)
    U_init = A * np.sin(np.pi * x * k)
    
    F_curr = lambda u: F(u, p, N)
    dF_curr= lambda u: dF(u, p, N)
    U = newton_multidim(U_init[1:-1], F_curr, dF_curr)
    U = np.append(U, 0)
    
    U = np.insert(U, 0, 0, axis=0)
    return x, U