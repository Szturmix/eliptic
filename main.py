import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt


from eliptic import visualisation
from eliptic import nmethods
  
## Part 2.1 Methode de tir ##  

N     = 1000
U0    = 0
sign  = -1
p     = 3
t     = np.linspace(0.01, 1.0, N)
ps    = [2, 3, 4]
sigmas = np.arange(-20.0, 20, 0.1)

# # Example of a solution
#  
# norms = np.zeros((len(ps),len(sigmas)))
# kappas= np.zeros((len(ps),len(sigmas)))
# sigma = 0.5
#   
# plt.grid()
# for idx, p in enumerate(ps):
#     sol = odeint(nmethods.f, [U0, sigma], t, args=(sign, p))
#     title = "p= " + str(ps[idx])
#     plt.plot(t ,sol[:,0] , '-', label=title)
#     
# plt.legend(loc="best")
# plt.xlabel('r')
# plt.ylabel('u(r)')
# plt.savefig("plus_sign_solution" + ".png")
# plt.show()
# plt.clf()


    
# Question 1) Eulers method, parametric plot of the norm
#    
# norms = np.zeros((len(ps),len(sigmas)))
# kappas= np.zeros((len(ps),len(sigmas)))
#      
# kappas, norms = nmethods.solve_with_euler(ps, U0, t, sigmas, sign)
#          
#visualisation.norm_subplot(norms, kappas, ps)
#visualisation.norm_plot(norms[0,:], kappas[0,:], sigmas[0], p)
   
## Question 2) Secants method for finding sigma for which u(1) = 0
   
#sigma = nmethods.secants_method(lambda x : nmethods.find_kappa(x, N, U0, sign, p, t), 3.0, 4.1, 0.1)
#sol = odeint(nmethods.f, [U0, 20], t, args=(sign, p))
#subtitle = "Using Secants method to find u(1)=0"
#visualisation.solution_plot(sol[:,0], t, 1, p, subtitle)


## Part 2.2 Methode utilisant l'invariance d'echelle ##  
## Visualization of several solutions                ##
# 
# sigma = 1.0
# us = [] 
# fig, ax = plt.subplots(3)
#  
# epsilon = 0.01
# p=2 
# for n in range(1,4):
#     t = np.linspace(0, 15.0, N)
#     sol = odeint(nmethods.f, [U0, sigma], t, args=(sign, p))
#     idx = nmethods.find_nearest(sol[:,0], 0.0, epsilon)
#     subtitle = "Before scaling"
#     #visualisation.solution_plot_with_zero(sol[:,0], t, sigma, p, idx, subtitle)
#    
#     subtitle = "Scaled solution with lambda=" + str(t[idx[n]])
#     t, u = nmethods.scale_solution(t, sol[:,0], p, t[idx[n]], epsilon, n)
#     ax[0].plot(t ,u , '-')
# 
# title = "p= " + str(p)
# ax[0].set_title(title)
# 
# epsilon = 0.01
# p=3 
# sigma = 0.5
# for n in range(1,4):
#     t = np.linspace(0, 15.0, N)
#     sol = odeint(nmethods.f, [U0, sigma], t, args=(sign, p))
#     idx = nmethods.find_nearest(sol[:,0], 0.0, epsilon)
#     subtitle = "Before scaling"
#     #visualisation.solution_plot_with_zero(sol[:,0], t, sigma, p, idx, subtitle)
#    
#     t, u = nmethods.scale_solution(t, sol[:,0], p, t[idx[n]], epsilon, n)
#     ax[1].plot(t ,u , '-')
# 
# title = "p= " + str(p)
# ax[1].set_title(title)
# 
# epsilon = 0.025
# p=7 
# sigma = 1
# for n in range(1,4):
#     t = np.linspace(0, 15.0, N)
#     sol = odeint(nmethods.f, [U0, sigma], t, args=(sign, p))
#     idx = nmethods.find_nearest(sol[:,0], 0.0, epsilon)
#     subtitle = "Before scaling"
#     #visualisation.solution_plot_with_zero(sol[:,0], t, sigma, p, idx, subtitle)
#    
#     t, u = nmethods.scale_solution(t, sol[:,0], p, t[idx[n]], epsilon, n)
#     ax[2].plot(t ,u , '-')
# 
# title = "p= " + str(p)
# ax[2].set_title(title)
# 
# plt.savefig("manysolutions.png")        
# plt.show()

#visualisation.solution_plot_with_zero(u, t, sigma, p, len(u)-1, sub_title=subtitle)

## 2.3 Methode des differences finis

# p = 5
# N = 202
# k = 3
# A = 3
# t, U = nmethods.solve_with_difference_method(p, N, k, A)
# visualisation.save_solution_plot(U, t, p, " difference method1 , k = " + str(k) + ", N = " + str(N) + ", A = " + str(A))
# 
# p = 6
# N = 202
# k = 3
# A = 3
# t, U = nmethods.solve_with_difference_method(p, N, k, A)
# visualisation.save_solution_plot(U, t, p, "difference method2, k = " + str(k) + ", N = " + str(N) + ", A = " + str(A))
# 
# p = 7
# N = 202
# k = 3
# A = 4
# t, U = nmethods.solve_with_difference_method(p, N, k, A)
# visualisation.save_solution_plot(U, t, p, "difference method3, k = " + str(k) + ", N = " + str(N) + ", A = " + str(A))

## 3.1 Methode de tri

# Question 10) Eulers method, parametric plot of the norm
# n = 2 
# norms = np.zeros((len(ps),len(sigmas)))
# kappas= np.zeros((len(ps),len(sigmas)))
#         
# kappas, norms = nmethods.solve_with_euler_n(ps, U0, t, sigmas, sign, n)
#             
# visualisation.norm_subplot(norms, kappas, ps)
 
## Secants method for finding sigma for which u(1) = 0
#       
# sigma = nmethods.secants_method(lambda x : nmethods.find_kappa_n(x, N, U0, sign, p, t, n), 120, 121, 0.1)
# print(sigma)
# sol = odeint(nmethods.g, [U0, sigma], t, args=(sign, p, n))
# subtitle = "Using Secants method to find u(1)=0"
# visualisation.solution_plot(sol[:,0], t, sigma, p, subtitle)

## Part 3.2 Methode utilisant l'invariance d'echelle Methodes Radiales ##
## Visualization of several solutions                
# n = 3
# p = 4
# N = 100000
# b = 0.0001
# sign = 1
# sigma = 1000
# t     = np.linspace(0.01, b, N)
# sol = odeint(nmethods.g, [sigma, 0], t, args=(sign, p, n))
# subtitle = "Using Secants method to find u(1)=0"
#   
#   
# epsilon = 0.0001
# idx = nmethods.find_nearest(sol[:,0], 0.0, epsilon)
# visualisation.solution_plot_with_zeros(sol[:,0], t, 1, p, idx, subtitle) 
# #subtitle = "Before scaling"
# print(idx)   
# subtitle = "Scaled solution with lambda=" + str(t[idx[n]])
  
# for i in range(3):
#     t     = np.linspace(0.01, b, N)
#     lbl   = "$\lambda=$" +str(t[idx[i]]) 
#     t, u  = nmethods.scale_solution(t, sol[:,0], p, t[idx[i]], epsilon, number=i)
#     plt.plot(t, u, label=lbl)    
# plt.grid()
# plt.legend()
# plt.savefig("p4n3sigma10.png")
# plt.show()

## Plot different solutions with euler for n>1

# n = 2 
# sigma = 1.0
# N = 2000
# t     = np.linspace(0.01, 100.0, N)
# for p in [1, 2, 3, 4]:
#     sol = odeint(nmethods.g, [U0, sigma], t, args=(sign, p, n))
#     subtitle = "Using Secants method to find u(1)=0"
#     lbl = "p=" + str(p)
#     plt.plot( t, sol[:,0], label=lbl)
# plt.savefig("Euler_solutions_n2.png")
# plt.legend()
# plt.show()

n = 3
t     = np.linspace(-10, 10.0, N)
for p in [3, 4 ,5 ,6]:
    V     = [ (4*p/(p-1)**2 - 2*n/(p-1)) * x + 1/p*np.abs(x)**p for x in t]
    plt.plot(t,V)
plt.show()